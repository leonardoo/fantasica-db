from __future__ import unicode_literals

__author__ = 'leonardoo'

from django.db import models
from django.utils.translation import ugettext as _
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.

@python_2_unicode_compatible
class UnitSkill(models.Model):

    name = models.CharField(verbose_name= _('Name'), max_length=50,unique=True)
    description = models.TextField(verbose_name= _('Description'),blank=True)

    class Meta:
        db_table = 'UnitSkill'

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class UnitSource(models.Model):

    name = models.CharField(verbose_name= _('Name'), max_length=50,unique=True)
    description = models.TextField(verbose_name= _('Description'),blank=True)

    class Meta:
        db_table = 'UnitSource'

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class UnitType(models.Model):

    name = models.CharField(verbose_name= _('Name'), max_length=10,unique=True)

    class Meta:
        db_table = 'UnitType'
        managed = False

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class UnitStar(models.Model):

    namee = models.CharField(verbose_name= _('Name'), max_length=10)
    img = models.ImageField(verbose_name= _('Star Img'),upload_to="stars")
    value = models.CharField(verbose_name= _('Value Star'), max_length=1)

    class Meta:
        db_table = 'UnitStar'

    def __str__(self):
        return self.value

@python_2_unicode_compatible
class UnitProfile(models.Model):

    name = models.CharField(verbose_name= _('Name'), max_length=250)
    unittype = models.ForeignKey(UnitType,verbose_name= _('Unit Type'))
    range = models.PositiveSmallIntegerField(verbose_name= _('Range'))
    speed = models.CharField(verbose_name= _('Speed'), max_length=1)
    delay = models.DecimalField(verbose_name= _('Delay'), decimal_places=2,max_digits=3)
    cost = models.PositiveSmallIntegerField(verbose_name= _('Cost'))
    stars = models.ForeignKey(UnitStar,verbose_name= _('Unit Star'))
    source = models.ManyToManyField(UnitSource,verbose_name= _('Source'))
    skill = models.ManyToManyField(UnitSkill,verbose_name= _('Skill'),null=True)

    class Meta:
        db_table = 'Unit'

    def __str__(self):
        return '{0}: {1}'.format(self.name,self.stars.value)

@python_2_unicode_compatible
class UnitLVL(models.Model):

    unit = models.ForeignKey(UnitProfile,verbose_name= _('Unit'))
    level = models.PositiveSmallIntegerField(verbose_name= _('Level'))
    ground = models.PositiveIntegerField(verbose_name= _('Ground'),default=0)
    air = models.PositiveIntegerField(verbose_name= _('Air'),default=0)
    water = models.PositiveIntegerField(verbose_name= _('Water'),default=0)
    Arena = models.PositiveIntegerField(verbose_name= _('Arena'),default=0)

    class Meta:
        db_table = 'UnitLevel'

    def __str__(self):
        return '{0}: {1}'.format(self.unit.name,self.level)