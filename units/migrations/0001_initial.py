# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UnitSkill'
        db.create_table('UnitSkill', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, unique=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('units', ['UnitSkill'])

        # Adding model 'UnitSource'
        db.create_table('UnitSource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, unique=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('units', ['UnitSource'])

        # Adding model 'UnitStar'
        db.create_table('UnitStar', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('namee', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('units', ['UnitStar'])

        # Adding model 'UnitProfile'
        db.create_table('Unit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('unittype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['units.UnitType'])),
            ('range', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('speed', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('delay', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('cost', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('stars', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['units.UnitStar'])),
        ))
        db.send_create_signal('units', ['UnitProfile'])

        # Adding M2M table for field source on 'UnitProfile'
        m2m_table_name = db.shorten_name('Unit_source')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('unitprofile', models.ForeignKey(orm['units.unitprofile'], null=False)),
            ('unitsource', models.ForeignKey(orm['units.unitsource'], null=False))
        ))
        db.create_unique(m2m_table_name, ['unitprofile_id', 'unitsource_id'])

        # Adding M2M table for field skill on 'UnitProfile'
        m2m_table_name = db.shorten_name('Unit_skill')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('unitprofile', models.ForeignKey(orm['units.unitprofile'], null=False)),
            ('unitskill', models.ForeignKey(orm['units.unitskill'], null=False))
        ))
        db.create_unique(m2m_table_name, ['unitprofile_id', 'unitskill_id'])

        # Adding model 'UnitLVL'
        db.create_table('UnitLevel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('unit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['units.UnitProfile'])),
            ('level', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('ground', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('air', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('water', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('Arena', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('units', ['UnitLVL'])


    def backwards(self, orm):
        # Deleting model 'UnitSkill'
        db.delete_table('UnitSkill')

        # Deleting model 'UnitSource'
        db.delete_table('UnitSource')

        # Deleting model 'UnitStar'
        db.delete_table('UnitStar')

        # Deleting model 'UnitProfile'
        db.delete_table('Unit')

        # Removing M2M table for field source on 'UnitProfile'
        db.delete_table(db.shorten_name('Unit_source'))

        # Removing M2M table for field skill on 'UnitProfile'
        db.delete_table(db.shorten_name('Unit_skill'))

        # Deleting model 'UnitLVL'
        db.delete_table('UnitLevel')


    models = {
        'units.unitlvl': {
            'Arena': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'Meta': {'db_table': "'UnitLevel'", 'object_name': 'UnitLVL'},
            'air': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ground': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['units.UnitProfile']"}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'units.unitprofile': {
            'Meta': {'db_table': "'Unit'", 'object_name': 'UnitProfile'},
            'cost': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'delay': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'range': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'skill': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['units.UnitSkill']", 'null': 'True', 'symmetrical': 'False'}),
            'source': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['units.UnitSource']", 'symmetrical': 'False'}),
            'speed': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'stars': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['units.UnitStar']"}),
            'unittype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['units.UnitType']"})
        },
        'units.unitskill': {
            'Meta': {'db_table': "'UnitSkill'", 'object_name': 'UnitSkill'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True'})
        },
        'units.unitsource': {
            'Meta': {'db_table': "'UnitSource'", 'object_name': 'UnitSource'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'unique': 'True'})
        },
        'units.unitstar': {
            'Meta': {'db_table': "'UnitStar'", 'object_name': 'UnitStar'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'namee': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'units.unittype': {
            'Meta': {'db_table': "'UnitType'", 'object_name': 'UnitType', 'managed': 'False'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '10', 'unique': 'True'})
        }
    }

    complete_apps = ['units']